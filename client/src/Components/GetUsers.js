import React, { useEffect, useState } from 'react'
import { useQuery, gql } from '@apollo/client'
import { LOAD_USERS } from '../GraphQL/Queries'

export default function GetUsers() {
    const { error, loading, data } = useQuery(LOAD_USERS)
    const [users, setUsers] = useState([])

    useEffect(() => {
        if(data) {
            setUsers(data.getAllUsers)
        }
       
       console.log(users)
    }, [data])

    return (
        <div>
            {users.map((val) => {
                return <h3>{val.firstName}</h3>
            })}
        </div>
    )
}
